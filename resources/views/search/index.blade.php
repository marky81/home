@extends('layout.layout')
@section('title', 'Search Films')

@section('description', 'Search films list')

@section('content')
@include('partial.jumbo')

<form url="films/search" method="GET" role="search">
    {{ csrf_field() }}
    <div class="input-group custom-search-form">
      <input type="text" class="form-control" name="search" placeholder="Search...">
      <span class="input-group-btn">
          <button class="btn btn-default-sm" type="submit">
              <i class="fa fa-search">find</i>
          </button>
      </span>
    </div>
</form>

<div class="display">
  <div class="box">
    @foreach ($films as $film)
    <div class="col">
      <h3>{{ $film->title }}</h3>
      <p>Runtime: {{ $film->runtime }} Minutes || Released: {{ $film->released }} || @include('partial.rating')</p>
      <p>{{$film->synopsis }}</p>
      <a class="btn btn-info" href="{{ url('/films/' . $film->id)}}">View</a>
    </div>
    @endforeach
  </div>
  {{ $films->links() }} <!-- pagination links -->
</div>



@endsection
