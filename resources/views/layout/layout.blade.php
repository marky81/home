<!DOCTYPE html>
<html lang="en">
<head>
  <title>@yield('title')</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="/css/app.css">
  <meta name="theme-color" content="#ffad2b">

  <!-- Facebook card -->
  <meta property=”og:title” content=”Markys little corner”/>
  <meta property=”og:type” content=”website”/>
  <meta property=”og:image” content=”https://pbs.twimg.com/profile_images/843841379495305216/WV2Kqrst_400x400.jpg”/>
  <meta property="og:description" content="@yield('description')"/>
  <meta property="og:url" content="{{ url(Request::path()) }}"/>

  <!-- Twitter card -->
  <meta name="twitter:card" content="summary"/>
  <meta name="twitter:site" content="@williamsmark81"/>
  <meta name="twitter:creator" content="Mark Williams"/>
  <meta name="twitter:title" content="Mark Williams Personal Page."/>
  <meta name="twitter:url" content="{{url(Request::path())}}"/>
  <meta name="twitter:description" content="@yield('description')"/>
  <meta name="twitter:image" content="https://pbs.twimg.com/profile_images/843841379495305216/WV2Kqrst_400x400.jpg"/>


  <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script> -->
  <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->
  <style>

  </style>
</head>
<body>

@include('partial.nav')

@yield('content')

@include('partial.foot')


</body>
</html>
