<nav role="navigation">
    <div class="bar">
      <ul class="menu">
        <li><a href="{{ url('/')}}">Home</a></li>
        <li><a href="{{ url('/films')}}">Films</a></li>
        <li><a href="{{ url('films/search')}}">Search</a></li>
        <li><a href="{{ url('/contact')}}">Contact</a></li>

        @if (Auth::guest())
            <li><a href="{{ route('login') }}">Login</a></li>
        @else
            <li><a href="{{ route('logout') }}"
                onclick="event.preventDefault();
                         document.getElementById('logout-form').submit();">
                Logout {{ Auth::user()->name }}?
            </a></li>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        @endif

      </ul>
      </div>
</nav>
