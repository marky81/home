@if($film->rating_id === 1)
  <img src="{{ url('/images/ratings/BBFC_U.png') }}" height="30px" alt="BBFC rating {{ $film->ratings->rating }}">
@elseif($film->rating_id === 2)
  <img src="{{ url('/images/ratings/BBFC_PG.png') }}" height="30px" alt="BBFC rating {{ $film->ratings->rating }}">
@elseif($film->rating_id === 3)
  <img src="{{ url('/images/ratings/BBFC_12a.png') }}" height="30px" alt="BBFC rating {{ $film->ratings->rating }}">
@elseif($film->rating_id === 4)
  <img src="{{ url('/images/ratings/BBFC_12.png') }}" height="30px" alt="BBFC rating {{ $film->ratings->rating }}">
@elseif($film->rating_id === 5)
  <img src="{{ url('/images/ratings/BBFC_15.png') }}" height="30px" alt="BBFC rating {{ $film->ratings->rating }}">
@else
  <img src="{{ url('/images/ratings/BBFC_18.png') }}" height="30px" alt="BBFC rating {{ $film->ratings->rating }}">
@endif
