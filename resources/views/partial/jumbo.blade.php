<div id="banner">
  <div class="container text-center">
    <h1>My Portfolio</h1>
    <p>Some text that represents "Me"...</p>
    <p>While your here, why not check out <a href="{{ url('/films/' . $post->id)}}">{{ $post->title }}</a></p>
  </div>
</div>
