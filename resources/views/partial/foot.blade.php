<footer class="container-fluid text-center">
<div class="foot">
  <ul>
    <li><a href="{{ url('/')}}">Home</a></li>
    <li><a href="{{ url('films') }}">Films</a></li>
    <li><a href="{{ url('contact') }}">Contact</a></li>
    <li><a href="{{ url('films/create')}}">Add New Film</a></li>
  </ul>
</div>
<div class="foot">
  <p>&copy; Mark Williams</p>
</div>
<div class="foot">
  <p>Mark is a graduate of Edge Hill University and graduated with a first class degree with honours in BSc (HONS) Web Design & Development</p>
</div>



</footer>
