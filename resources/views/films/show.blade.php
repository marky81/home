@extends('layout.layout')
@section('title', 'Film Details for $film>title')


@section('description', 'Detailed information on ' . $film->title)

@section('content')
@include('partial.jumbo')

<div class="display">

  <div class="plaque">
    <h1>{{$film->title}} @include('partial.rating')</h1>
    <p>{{$film->runtime}} minutes</p>
    <p>{{$film->synopsis}}</p>
  </div>

</div>




@endsection
