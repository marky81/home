@extends('layout.layout')

@section('content')

<form class="form-horizontal" method="POST" action="/films" >
   {{ csrf_field() }}
<fieldset>

<!-- Form Name -->
<legend>Add Film</legend>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="title">Title</label>
  <div class="col-md-4">
  <input id="title" name="title" type="text" placeholder="title..." class="form-control input-md" required="">

  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="runtime">Runtime</label>
  <div class="col-md-2">
  <input id="runtime" name="runtime" type="text" placeholder="Time in minutes..." class="form-control input-md" required="">

  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="released">Year Released</label>
  <div class="col-md-2">
  <input id="released" name="released" type="text" placeholder="Year released..." class="form-control input-md">

  </div>
</div>

<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="rating">Age Rating</label>
  <div class="col-md-4">
    <select id="rating" name="rating" class="form-control">
      @foreach($ratings as $rating)
      <option value="{{$rating->id}}">{{$rating->rating}}</option>
      @endforeach
    </select>
  </div>
</div>

<!-- Textarea -->
<div class="form-group">
  <label class="col-md-4 control-label" for="synopsis">Synopsis</label>
  <div class="col-md-4">
    <textarea class="form-control" id="synopsis" name="synopsis"></textarea>
  </div>
</div>

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="submit"></label>
  <div class="col-md-4">
    <button id="submit" name="submit" class="btn btn-primary">Submit</button>
  </div>
</div>

</fieldset>
</form>

<form>
  @if(count($errors))
    <div class="content flex-center">
        <ul class="alert">
          @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
          @endforeach
        </ul>
    </div>
  @endif
</form>

@endsection
