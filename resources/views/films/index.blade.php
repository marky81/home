@extends('layout.layout')
@section('title', 'Film listings')
@section('content')
@include('partial.jumbo')

<div class="display">
  <div class="box">
    @foreach ($films as $film)
    <div class="col">
      <h3>{{ $film->title }}</h3>
      <p>Runtime: {{ $film->runtime }} Minutes || Released: {{ $film->released }} || @include('partial.rating')</p>
      <p>{{$film->synopsis }}</p>
      <a class="btn btn-info" href="{{ url('/films/' . $film->id)}}">View</a>
    </div>
    @endforeach
  </div>
</div>

@endsection
