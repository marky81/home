<?php
use Illuminate\Support\Facades\Input;
use App\Film;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', 'GeneralController@index');

Route::get('films', 'FilmController@index');
Route::get('films/create', 'FilmController@create')->middleware('auth');

Route::get('films/search', 'SeachController@index');

Route::get('films/{film}', 'FilmController@show');
Route::post('films', 'FilmController@store');

Auth::routes();

Route::get('/home', 'HomeController@index');
