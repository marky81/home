<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class film extends Model
{

  protected $fillable = [
    'title','synopsis','runtime','released','rating'
   ];

    public function ratings()
    {
      return $this->belongsTo(rating::class);
    }
}
