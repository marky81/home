<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Film extends Model
{
    public function ratings()
    {
      return $this->belongsTo('App\Rating', 'rating_id', 'id');
    }

    public function peoples()
    {
      return $this->belongsToMany('App\People', 'film_peoples', 'films_id', 'peoples_id');
    }
}
