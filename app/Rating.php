<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{
  public function films()
  {
    return $this->hasMany('App\Film', 'id', 'rating_id');
  }
}
