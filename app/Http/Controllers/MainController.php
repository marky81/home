<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Film;

class MainController extends Controller
{
  public function index()
  {
    $scan = Film::all();
    $len = count($scan);
    $ran = rand(0, $len-1);
    $post = Film::find($ran);
    return view('welcome', 'post');
  }
}
