<?php

namespace App\Http\Controllers;

use App\Film;
use App\Rating;
use Illuminate\Http\Request;

class FilmController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $films = Film::all()->sortBy("title");
        $len = count($films);
        $ran = rand(1, $len);
        $post = Film::find($ran);
        return view('films.index', compact('films', 'post'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $films = Film::all();
      $ratings = Rating::all();
    return view('films.create', compact('films', 'ratings'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      // validate data. Regex ensures 3 uppercase letters followed by 4 numbers only.
      $this->validate(request(), [
      'title' => 'required',
      'synopsis' => 'required',
      'runtime' => 'required',
      'released' => 'required',
      'rating' => 'required'
      ]);

      // dd(request()->all());

      $submit = new film;

      // Database entry = form name
      $submit->title = request('title');;
      $submit->synopsis = request('synopsis');
      $submit->runtime = request('runtime');
      $submit->released = request('released');
      $submit->rating_id = request('rating');

      // Save data to database
      $submit->save();

      // Redirect to page when submitted
      return redirect('films');


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\film  $film
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $film = Film::find($id);

      $scan = Film::all()->sortBy("title");
      $len = count($scan);
      $ran = rand(1, $len);
      $post = Film::find($ran);


      return view('films.show', compact('film', 'post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\film  $film
     * @return \Illuminate\Http\Response
     */
    public function edit(film $film)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\film  $film
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, film $film)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\film  $film
     * @return \Illuminate\Http\Response
     */
    public function destroy(film $film)
    {
        //
    }
}
