<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class People extends Model
{
    public function films()
    {
      return $this->belongsToMany('App\Film', 'film_peoples', 'films_id', 'peoples_id');
    }
}
