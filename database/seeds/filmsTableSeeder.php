<?php

use Illuminate\Database\Seeder;

class filmsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('films')->insert([
        array('title' => 'Big Hero 6', 'synopsis' => 'The special bond that develops between plus-sized inflatable robot Baymax, and prodigy Hiro Hamada, who team up with a group of friends to form a band of high-tech heroes.', 'runtime' => '102', 'released' => '2014', 'rating_id' => '2'),
        array('title' => 'Police Academy', 'synopsis' => 'A group of good-hearted but incompetent misfits enter the police academy, but the instructors there are not going to put up with their pranks.', 'runtime' => '96', 'released' => '1984', 'rating_id' => '5'),
        array('title' => 'The Imitation Game', 'synopsis' => 'During World War II, mathematician Alan Turing tries to crack the enigma code with help from fellow mathematicians.', 'runtime' => '114', 'released' => '2014', 'rating_id' => '3')

      ]);
    }
}
