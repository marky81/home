<?php

use Illuminate\Database\Seeder;

class ratingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
     public function run()
    {
        DB::table('ratings')->insert([
          array('rating' => 'U'),
          array('rating' => 'PG'),
          array('rating' => '12A'),
          array('rating' => '12'),
          array('rating' => '15'),
          array('rating' => '18')
        ]);
    }
}
