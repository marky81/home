<?php

use Illuminate\Database\Seeder;

class PeoplesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('peoples')->insert([
        array('forename' => 'Daniel', 'surname' => 'Henney', 'gender' => 'm'),
        array('forename' => 'Todd Joseph', 'surname' => 'Miller', 'gender' => 'm'),
        array('forename' => 'Damon', 'surname' => 'Wayans', 'gender' => 'm'),
        array('forename' => 'Steve', 'surname' => 'Guttenberg', 'gender' => 'm'),
        array('forename' => 'Kim', 'surname' => 'Cattrall', 'gender' => 'f'),
        array('forename' => 'George William', 'surname' => 'Bailey', 'gender' => 'm'),
        array('forename' => 'Bubba', 'surname' => 'Smith', 'gender' => 'm'),
        array('forename' => 'George ', 'surname' => 'Gaynes', 'gender' => 'm'),
        array('forename' => 'David', 'surname' => 'Graf', 'gender' => 'm'),
        array('forename' => 'Leslie', 'surname' => 'Easterbrook', 'gender' => 'f'),
        array('forename' => 'Michael', 'surname' => 'Winslow', 'gender' => 'm'),
        array('forename' => 'Benedict', 'surname' => 'Cumberbatch', 'gender' => 'm'),
        array('forename' => 'Keira', 'surname' => 'Knightley', 'gender' => 'f'),
        array('forename' => 'Matthew', 'surname' => 'Goode', 'gender' => 'm'),
        array('forename' => 'Roy', 'surname' => 'Kinnear', 'gender' => 'm'),
        array('forename' => 'Charles', 'surname' => 'Dance', 'gender' => 'm'),
        array('forename' => 'Mark', 'surname' => 'Strong', 'gender' => 'm'),
        array('forename' => 'Eddie', 'surname' => 'Redmayne', 'gender' => 'm'),
        array('forename' => 'Sam', 'surname' => 'Redford', 'gender' => 'm'),
        array('forename' => 'Colin', 'surname' => 'Farrell', 'gender' => 'm'),
        array('forename' => 'Samantha', 'surname' => 'Morton', 'gender' => 'f'),
        array('forename' => 'Michael', 'surname' => 'Winslow', 'gender' => 'm'),
        array('forename' => 'Dan', 'surname' => 'Fogler', 'gender' => 'm'),
      ]);
    }
}
