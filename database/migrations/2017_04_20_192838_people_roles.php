<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PeopleRoles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('people_roles', function (Blueprint $table) {
        $table->integer('peoples_id')->unsigned();
        $table->foreign('peoples_id')->references('id')->on('peoples')->onDelete('cascade');
        $table->integer('roles_id')->unsigned();
        $table->foreign('roles_id')->references('id')->on('roles')->onDelete('cascade');

        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('people_roles');
    }
}
