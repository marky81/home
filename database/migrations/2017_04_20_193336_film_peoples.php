<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FilmPeoples extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('film_peoples', function (Blueprint $table) {
        $table->integer('films_id')->unsigned();
        $table->foreign('films_id')->references('id')->on('films')->onDelete('cascade');
        $table->integer('peoples_id')->unsigned();
        $table->foreign('peoples_id')->references('id')->on('peoples')->onDelete('cascade');

        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('film_peoples');
    }
}
